import envSchema from 'env-schema';
import { config as envConfig } from './environmentVariables.js';

const config = envSchema(envConfig);

const {
  DB_HOST,
  DB_USER,
  DB_PASSWORD,
  DB_NAME,
  DB_PORT
} = config;

const dbConfig = {
  client: 'postgres',
  connection: {
    host: DB_HOST,
    user: DB_USER,
    password: DB_PASSWORD,
    database: DB_NAME,
    port: DB_PORT,
  },
};

export default dbConfig;
