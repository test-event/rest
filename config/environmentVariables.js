const schema = {
  type: 'object',
  properties: {
    HOST: {
      type: 'string',
      default: '0.0.0.0',
    },
    PORT: {
      type: 'number',
      default: '1337',
    },
    // db
    DB_HOST: {
      type: 'string',
    },
    DB_USER: {
      type: 'string',
    },
    DB_PASSWORD: {
      type: 'string',
    },
    DB_NAME: {
      type: 'string',
    },
    DB_PORT: {
      type: 'string',
    },
  },
};

export const config = {
  dotenv: true,
  schema,
};
