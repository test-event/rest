const camelToSnakeCase = (str) => str.replace(/[A-Z]/g, letter => `_${letter}`).toUpperCase();

const validationError = (error, context) => {
  const { keyword, message, params, instancePath } = error;
  let field = null;
  let errMessage = null;
  let code = null;

  const allowedValues =
    params && Array.isArray(params.allowedValues)
      ? `: ${params.allowedValues.map(v => `'${v}'`).join(', ')}`
      : '';

  if (keyword === 'required') {
    field = params.missingProperty;
    code = 'REQUIRED';
    errMessage = `body ${message}${allowedValues}`;
  } else {
    field = instancePath.replace('/', '');
    code = camelToSnakeCase(keyword);
    errMessage = `${field} ${message}${allowedValues}`;
  }

  return { code, message: errMessage, field };
};

export const errorHandler = (error, request, reply) => {
  if (Array.isArray(error.validation)) {
    const body = {
      message: 'REQUEST_VALIDATION_ERROR',
      extensions: {
        fields: error.validation.map(err => validationError(err, error.validationContext)),
      },
    };

    reply.code(400);
    reply.type('application/json');
    reply.send(body);
  }
};
