import envSchema from 'env-schema';
import { config as envConfig } from '../config/environmentVariables.js';

import { init, run } from './server.js';

(async () => {
  const config = envSchema(envConfig);

  try {
    const server = await init({ config });
    await run(server);
  } catch (error) {
    console.error(error, 'Error While Starting the Server');
  }
})();
