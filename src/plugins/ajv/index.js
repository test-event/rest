import fp from 'fastify-plugin';
import Ajv from 'ajv';
import AjvErrors from 'ajv-errors';
import addFormat from 'ajv-formats';

const ajvCompiler = async (fastify, options) => {
  const ajv = new Ajv({
    removeAdditional: true,
    useDefaults: true,
    coerceTypes: true,
    allErrors: true,
    allowUnionTypes: true,
  });

  AjvErrors(ajv);
  addFormat(ajv);

  fastify.setValidatorCompiler(({ schema }) => ajv.compile(schema));
};

export default fp(ajvCompiler, {
  fastify: '>=4.0.0',
  name: 'ajv-compiler',
});
