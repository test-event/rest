import knex from 'knex';

const connectionCheck = db => db.raw(`--sql
  SELECT 1 + 1 AS result
`);

const getKnexClient = async ({ options }) => {
  try {
    const db = knex({ ...options });
    await connectionCheck(db);

    return db;
  } catch (error) {
    console.error({ message: 'DB connection failed', error });
    throw Error(`Connection Failed ${ error }`);
  }
};

export default getKnexClient;
