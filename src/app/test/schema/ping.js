import { errorSchemas } from '../../common/schema.js';

const request = {
  tags: ['ping'],
  summary: 'Return message',
  description: `<h3> This API return message </h3>`,
};

const response = {
  type: 'object',
  required: ['message'],
  properties: {
    message: {
      type: 'string',
    },
  },
};



export const schema = {
  ...request,
  response: {
    200: response,
    ...errorSchemas,
  },
};
