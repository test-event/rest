export const handler = async function (request, reply) {
  const { message } = request.query;

  console.info(message);

  reply
    .status(200)
    .send({
      message,
    });
};
