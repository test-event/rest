import { schema as pingSchema } from './schema/ping.js';
import { handler as pingHandler } from './handlers/ping.js';

export default async (fastify) => {
  fastify.route({
    method: 'GET',
    url: '/ping',
    schema: pingSchema,
    handler: pingHandler,
  });
};
