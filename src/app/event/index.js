import { getAllEventsSchema } from './schema/getAllEvents.js';
import { getAllEventsController } from './controller/getAllEvents.js';

import { getEventSchema } from './schema/getEvent.js';
import { getEventController } from './controller/getEvent.js';

import { addEventSchema } from './schema/addEvent.js';
import { addEventController } from './controller/addEvent.js';

import { updateEventSchema } from './schema/updateEvent.js';
import { updateEventController } from './controller/updateEvent.js';

import { deleteEventSchema } from './schema/deleteEvent.js';
import { deleteEventController } from './controller/deleteEvent.js';

export default async (fastify) => {
  fastify.route({
    method: 'GET',
    url: '/',
    schema: getAllEventsSchema,
    handler: getAllEventsController,
  });
  fastify.route({
    method: 'GET',
    url: '/:id',
    schema: getEventSchema,
    handler: getEventController,
  });
  fastify.route({
    method: 'POST',
    url: '/',
    schema: addEventSchema,
    handler: addEventController,
  });
  fastify.route({
    method: 'PUT',
    url: '/:id',
    schema: updateEventSchema,
    handler: updateEventController,
  });
  fastify.route({
    method: 'DELETE',
    url: '/:id',
    schema: deleteEventSchema,
    handler: deleteEventController,
  });
};
