import { errorSchemas } from "../../common/schema.js";

const request = {
  tags: ['events'],
  summary: 'Return all events',
  description: `<h3> To get the response that contains all the events </h3>`,
};

const response = {
  type: 'array',
  items: {
    type: 'object',
    required: ['id', 'title'],
    properties: {
      id: {
        type: 'number',
      },
      title: {
        type: 'string',
      },
    }
  },
};

export const getAllEventsSchema = {
  ...request,
  response: {
    200: response,
    ...errorSchemas,
  }
};
