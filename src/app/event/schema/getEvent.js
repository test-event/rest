import { errorSchemas } from "../../common/schema.js";

const request = {
  tags: ['event'],
  summary: 'Return event by id',
  description: `<h3> To get a particular book from the id of the event </h3>`,
  params: {
    id: {
      type: 'number'
    },
  },
};

const response = {
  type: 'object',
  required: ['id', 'title'],
  properties: {
    id: {
      type: 'number',
    },
    title: {
      type: 'string',
    },
  },
};

export const getEventSchema = {
  ...request,
  response: {
    200: response,
    ...errorSchemas,
  }
};
