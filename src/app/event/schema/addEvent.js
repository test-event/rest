import { errorSchemas } from "../../common/schema.js";

const request = {
  tags: ['event', 'add'],
  summary: 'Add event',
  description: `<h3> To add a event to the event array of event objects. </h3>`,
  body: {
    type: 'object',
    required: ['title'],
    properties: {
      title: {
        type: 'string',
        minLength: 3,
      },
    },
  },
};

const response = {
  type: 'object',
    required: ['rowCount'],
    properties: {
      rowCount: {
        type: 'number',
      },
    },
};

export const addEventSchema = {
  ...request,
  response: {
    200: response,
    ...errorSchemas,
  },
};
