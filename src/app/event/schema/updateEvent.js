import { errorSchemas } from "../../common/schema.js";

const request = {
  tags: ['events', 'put'],
  summary: 'Update event by id',
  description: `<h3> To update a event </h3>`,
  params: {
    id: {
      type: 'number'
    },
  },
  body: {
    type: 'object',
    properties: {
      title: {
        type: 'string',
        minLength: 3,
      },
    },
  },
};

const response = {
  type: 'object',
  required: ['rowCount'],
  properties: {
    rowCount: {
      type: 'number',
    },
  },
};

export const updateEventSchema = {
  ...request,
  response: {
    200: response,
    ...errorSchemas,
  }
};
