import { errorSchemas } from "../../common/schema.js";

const request = {
  tags: ['events', 'delete'],
  summary: 'Delete event by id',
  description: `<h3> To delete a event </h3>`,
  params: {
    id: {
      type: 'number'
    },
  },
};

const response = {};

export const deleteEventSchema = {
  ...request,
  response: {
    200: response,
    ...errorSchemas,
  }
};
