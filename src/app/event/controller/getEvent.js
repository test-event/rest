import { getEventService } from '../service/getEvent.js';

export const getEventController = async function (request, reply) {
  try {
    const { id } = request.params;

    const event = await getEventService(this, id);

    reply
      .status(200)
      .send(event);
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
};
