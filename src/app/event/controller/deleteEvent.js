import { deleteEventService } from '../service/deleteEvent.js';

export const deleteEventController = async function (request, reply) {
  try {
    const { id } = request.params;

    await deleteEventService(this, id);

    reply
      .status(204)
      .send();
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
};
