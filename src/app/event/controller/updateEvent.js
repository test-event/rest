import { updateEventService } from '../service/updateEvent.js';

export const updateEventController = async function (request, reply) {
  try {
    const { id } = request.params;
    const { body } = request;

    const rowCount = await updateEventService(this, id, body);

    reply
      .status(200)
      .send(rowCount);
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
};
