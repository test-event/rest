import { getAllEventsService } from '../service/getAllEvents.js';

export const getAllEventsController = async function (request, reply) {
  try {
    const events = await getAllEventsService(this);

    reply
      .status(200)
      .send(events);
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
};
