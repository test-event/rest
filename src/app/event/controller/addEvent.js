import { addEventService } from '../service/addEvent.js';

export const addEventController = async function (request, reply) {
  try {
    const { body } = request;

    const rowCount = await addEventService(this, body);

    reply
      .status(201)
      .send(rowCount);
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
};
