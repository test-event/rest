export const addEventService = async (fastify, { title }) => {
  try {
    const events = await fastify.knex.raw(`--sql
      INSERT INTO public.event(title) VALUES (:title);
    `, {
      title: title.trim(),
    });

    const { rowCount } = events;

    return {
      rowCount,
    };
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
};
