export const deleteEventService = async (fastify, id) => {
  try {
    await fastify.knex.raw(`--sql
      DELETE FROM public.event WHERE id = :id;
    `, {
      id,
    });
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
};
