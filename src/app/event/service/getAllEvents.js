export const getAllEventsService = async (fastify) => {
  try {
    const events = await fastify.knex.raw(`--sql
      SELECT id, title FROM public.event;
    `);

    const { rows } = events;

    return rows;
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
};
