import { getEventService as getEvent } from "./getEvent.js";

export const updateEventService = async (fastify, id, { title }) => {
  try {
    const oldEvent = await getEvent(fastify, id);

    const newObject = {
      title: title !== undefined ? title.trim() : oldEvent.title,
    };

    const newEvent = await fastify.knex.raw(`--sql
      UPDATE public.event
        SET title = :title
        WHERE id = :id;
    `, {
      ...oldObject,
      id,
    });

    const { rowCount } = newEvent;

    return {
      rowCount,
    };
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
};
