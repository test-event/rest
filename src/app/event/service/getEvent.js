export const getEventService = async (fastify, id) => {
  try {
    const event = await fastify.knex.raw(`--sql
      SELECT id, title FROM public.event
      WHERE id = :id;
    `, {
      id,
    });

    const row = event.rows[0];

    return row ? row : {};
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
};
