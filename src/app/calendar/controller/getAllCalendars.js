import { getAllCalendarsService } from '../service/getAllCalendars.js';

export const getAllCalendarsController = async function (request, reply) {
  try {
    const calendars = await getAllCalendarsService(this);

    reply
      .status(200)
      .send(calendars);
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
};
