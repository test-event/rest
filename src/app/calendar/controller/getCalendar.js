import { getCalendarService } from '../service/getCalendar.js';

export const getCalendarController = async function (request, reply) {
  try {
    const { id } = request.params;

    const event = await getCalendarService(this, id);

    reply
      .status(200)
      .send(event);
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
};
