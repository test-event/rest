import { updateCalendarService } from '../service/updateCalendar.js';

export const updateCalendarController = async function (request, reply) {
  try {
    const { id } = request.params;
    const { body } = request;

    const rowCount = await updateCalendarService(this, id, body);

    reply
      .status(200)
      .send(rowCount);
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
};
