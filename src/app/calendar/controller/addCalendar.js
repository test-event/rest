import { addCalendarService } from '../service/addCalendar.js';

export const addCalendarController = async function (request, reply) {
  try {
    const { datetime_from, datetime_to, event_id } = request.body;

    console.log(datetime_to);

    const rowCount = await addCalendarService(this, {
      datetime_from,
      datetime_to: datetime_to ? datetime_to : null,
      event_id,
    });

    reply
      .status(201)
      .send(rowCount);
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
};
