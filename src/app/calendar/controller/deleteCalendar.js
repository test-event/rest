import { deleteCalendarService } from '../service/deleteCalendar.js';

export const deleteCalendarController = async function (request, reply) {
  try {
    const { id } = request.params;

    await deleteCalendarService(this, id);

    reply
      .status(204)
      .send();
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
};
