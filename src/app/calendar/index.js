import { getAllCalendarsSchema } from './schema/getAllCalendars.js';
import { getAllCalendarsController } from './controller/getAllCalendars.js';

import { getCalendarSchema } from './schema/getCalendar.js';
import { getCalendarController } from './controller/getCalendar.js';

import { addCalendarSchema } from './schema/addCalendar.js';
import { addCalendarController } from './controller/addCalendar.js';

import { updateCalendarSchema } from './schema/updateCalendar.js';
import { updateCalendarController } from './controller/updateCalendar.js';

import { deleteCalendarSchema } from './schema/deleteCalendar.js';
import { deleteCalendarController } from './controller/deleteCalendar.js';

export default async (fastify) => {
  fastify.route({
    method: 'GET',
    url: '/',
    schema: getAllCalendarsSchema,
    handler: getAllCalendarsController,
  });
  fastify.route({
    method: 'GET',
    url: '/:id',
    schema: getCalendarSchema,
    handler: getCalendarController,
  });
  fastify.route({
    method: 'POST',
    url: '/',
    schema: addCalendarSchema,
    handler: addCalendarController,
  });
  fastify.route({
    method: 'PUT',
    url: '/:id',
    schema: updateCalendarSchema,
    handler: updateCalendarController,
  });
  fastify.route({
    method: 'DELETE',
    url: '/:id',
    schema: deleteCalendarSchema,
    handler: deleteCalendarController,
  });
};
