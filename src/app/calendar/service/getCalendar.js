export const getCalendarService = async (fastify, id) => {
  try {
    const calendar = await fastify.knex.raw(`--sql
      SELECT id, datetime_from, datetime_to, event_id FROM public.calendar
      WHERE id = :id;
    `, {
      id,
    });

    const row = calendar.rows[0];

    return row ? row : {};
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
};
