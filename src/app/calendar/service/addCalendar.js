export const addCalendarService = async (fastify, { datetime_from, datetime_to, event_id }) => {
  try {
    const calendar = await fastify.knex.raw(`--sql
      INSERT INTO public.calendar(datetime_from, datetime_to, event_id) VALUES (:datetime_from, :datetime_to, :event_id);
    `, {
      datetime_from,
      datetime_to,
      event_id,
    });

    const { rowCount } = calendar;

    return {
      rowCount,
    };
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
};
