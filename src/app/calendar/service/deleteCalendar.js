export const deleteCalendarService = async (fastify, id) => {
  try {
    await fastify.knex.raw(`--sql
      DELETE FROM public.calendar WHERE id = :id;
    `, {
      id,
    });
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
};
