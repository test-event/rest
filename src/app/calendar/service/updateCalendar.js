import { getCalendarService as getCalendar } from "./getCalendar.js";

export const updateCalendarService = async (fastify, id, { datetime_from, datetime_to, event_id }) => {
  try {
    const oldCalendar = await getCalendar(fastify, id);

    const newObject = {
      datetime_from: datetime_from ? datetime_from : oldCalendar.datetime_from,
      datetime_to: datetime_to ? datetime_to : oldCalendar.datetime_to,
      event_id: event_id ? event_id : oldCalendar.event_id,
    };

    const calendar = await fastify.knex.raw(`--sql
      UPDATE public.calendar
        SET datetime_from = :datetime_from, datetime_to = :datetime_to, event_id = :event_id
        WHERE id = :id;
    `, {
      ...newObject,
      id,
    });

    const { rowCount } = calendar;

    return {
      rowCount,
    };
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
};
