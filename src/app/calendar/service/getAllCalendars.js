export const getAllCalendarsService = async (fastify) => {
  try {
    const calendars = await fastify.knex.raw(`--sql
      SELECT id, datetime_from, datetime_to, event_id FROM public.calendar;
    `);

    const { rows } = calendars;

    return rows;
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
};
