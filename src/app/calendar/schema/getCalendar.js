import { errorSchemas } from "../../common/schema.js";

const request = {
  tags: ['calendar'],
  summary: 'Return calendar by id',
  description: `<h3> To get a particular calendar from the id of the calendar </h3>`,
  params: {
    id: {
      type: 'number'
    },
  },
};

const response = {
  type: 'object',
  required: ['id', 'datetime_from', 'datetime_to', 'event_id'],
  properties: {
    id: {
      type: 'number',
    },
    datetime_from: {
      type: 'string',
    },
    datetime_to: {
      type: 'string',
    },
    event_id: {
      type: 'number',
    },
  },
};

export const getCalendarSchema = {
  ...request,
  response: {
    200: response,
    ...errorSchemas,
  }
};
