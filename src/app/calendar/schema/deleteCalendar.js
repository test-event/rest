import { errorSchemas } from "../../common/schema.js";

const request = {
  tags: ['calendar', 'delete'],
  summary: 'Delete calendar by id',
  description: `<h3> To delete a calendar </h3>`,
  params: {
    id: {
      type: 'number'
    },
  },
};

const response = {};

export const deleteCalendarSchema = {
  ...request,
  response: {
    200: response,
    ...errorSchemas,
  }
};
