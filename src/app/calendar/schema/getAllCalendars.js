import { errorSchemas } from "../../common/schema.js";

const request = {
  tags: ['calendars'],
  summary: 'Return all calendars',
  description: `<h3> To get the response that calendar all the calendars </h3>`,
};

const response = {
  type: 'array',
  items: {
    type: 'object',
    required: ['id', 'datetime_from', 'datetime_to', 'event_id'],
    properties: {
      id: {
        type: 'number',
      },
      datetime_from: {
        type: 'string',
      },
      datetime_to: {
        type: 'string',
      },
      event_id: {
        type: 'number',
      },
    },
  },
};

export const getAllCalendarsSchema = {
  ...request,
  response: {
    200: response,
    ...errorSchemas,
  }
};
