import { errorSchemas } from "../../common/schema.js";

const request = {
  tags: ['calendar', 'put'],
  summary: 'Update calendar by id',
  description: `<h3> To update a calendar </h3>`,
  params: {
    id: {
      type: 'number'
    },
  },
  body: {
    type: 'object',
    properties: {
      datetime_from: {
        type: 'string',
      },
      datetime_to: {
        type: 'string',
      },
      event_id: {
        type: 'number',
      }
    },
  },
};

const response = {
  type: 'object',
  required: ['rowCount'],
  properties: {
    rowCount: {
      type: 'number',
    },
  },
};

export const updateCalendarSchema = {
  ...request,
  response: {
    200: response,
    ...errorSchemas,
  }
};
