import { errorSchemas } from "../../common/schema.js";

const request = {
  tags: ['calendar', 'add'],
  summary: 'Add calendar',
  description: `<h3> To add a calendar to the event array of calendar objects. </h3>`,
  body: {
    type: 'object',
    required: ['datetime_from', 'event_id'],
    properties: {
      datetime_from: {
        type: 'string',
      },
      datetime_to: {
        type: 'string',
      },
      event_id: {
        type: 'number',
      }
    },
  },
};

const response = {
  type: 'object',
    required: ['rowCount'],
    properties: {
      rowCount: {
        type: 'number',
      },
    },
};

export const addCalendarSchema = {
  ...request,
  response: {
    200: response,
    ...errorSchemas,
  },
};
