import { errorHandler } from '../errors/handler.js';
import testRoutes from './test/index.js';
import eventRoutes from './event/index.js';
import calendarRoutes from './calendar/index.js';

export default async (fastify) => {
  fastify.setErrorHandler(errorHandler);
  fastify.register(testRoutes, { prefix: '/test' });
  fastify.register(eventRoutes, { prefix: '/event' });
  fastify.register(calendarRoutes, { prefix: '/calendar' });
};
