import fastify from 'fastify';
import * as routes from './app/routes.js';
import cors from '@fastify/cors';
import autoload from '@fastify/autoload';
import fastifyFormbody from '@fastify/formbody';
import fastifyFileUpload from 'fastify-file-upload';
import dayjs from 'dayjs';
import path from 'path';
import { fileURLToPath } from 'url';
import { v4 as uuid } from 'uuid';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const init = async ({ config }) => {
  const app = fastify({
    logger: true,
    genReqId: req => req.headers['x-request-id'] || uuid(),
    disableRequestLogging: false,
  });

  app.decorate('config', config);
  app.decorate('dayjs', dayjs);

  app.register(cors);
  app.register(fastifyFormbody);
  app.register(autoload, {
    dir: path.join(__dirname, 'plugins'),
  });
  app.register(fastifyFileUpload);
  app.register(routes);

  await app.ready();

  console.info('🚀 Everything is Loaded..!');

  return app;
};

const run = (app) => app.listen({
  port: app.config.PORT,
  host: app.config.HOST,
});

export { init, run };
